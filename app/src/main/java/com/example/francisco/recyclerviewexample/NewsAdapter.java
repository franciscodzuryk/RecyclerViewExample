package com.example.francisco.recyclerviewexample;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

class NewsAdapter extends RecyclerView.Adapter <NewsAdapter.NewsViewHolder> {
    private List<News> newsList;
    public NewsAdapter(List<News> newsList) {
        super();
        this.newsList = newsList;
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_news, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new NewsViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        News news = this.newsList.get(position);
        holder.headlineTextView.setText(news.headline);
        holder.bylineTextView.setText(news.byline);
        holder.leadTextView.setText(news.lead);

        new DownloadImageTask(holder.newsImageView).execute(news.imageURL);
    }

    @Override
    public int getItemCount() {
        return this.newsList.size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {
        public TextView headlineTextView;
        public TextView bylineTextView;
        public TextView leadTextView;
        public ImageView newsImageView;

        public NewsViewHolder(View itemView) {
            super(itemView);
            this.headlineTextView = itemView.findViewById(R.id.headlineTextView);
            this.bylineTextView = itemView.findViewById(R.id.bylineTextView);
            this.leadTextView = itemView.findViewById(R.id.leadTextView);
            this.newsImageView = itemView.findViewById(R.id.newsImageView);
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bmp = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                bmp = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bmp;
        }
        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
