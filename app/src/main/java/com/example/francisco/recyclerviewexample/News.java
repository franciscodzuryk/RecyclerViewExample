package com.example.francisco.recyclerviewexample;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class News {
    //Titulo de la noticia.
    public String headline;
    //Información del autor y fecha.
    public String byline;
    //Parrafo a modo de subtitulo.
    public String lead;
    //Noticia completa.
    public String body;
    //URL de la imagen
    public String imageURL;

    public News(String headline, String byline, String lead, String body, String imageURL) {
        this.headline = headline;
        this.byline = byline;
        this.lead = lead;
        this.body = body;
        if (imageURL == null)
            this.imageURL = "https://gitlab.com/franciscodzuryk/RecyclerViewExample/uploads/222db2d69b895550ccfbaabfbc5e601e/fidel_castro_mirada.jpg";
        else
            this.imageURL = imageURL;
    }

    public static List<News> getNewsList() {
        ArrayList<News> result = new ArrayList<>();

        for (int i = 10; i < 110; i++) {
            int d =  i % 10;
            switch (d) {
                case 0: result.add(new News("Head 0", "byline", "lead", "body", null));
                        break;
                case 1: result.add(new News("Head 1", "byline", "lead", "body", null));
                        break;
                case 2: result.add(new News("Head 2", "byline", "lead", "body", null));
                        break;
                case 3: result.add(new News("Head 3", "byline", "lead", "body", null));
                        break;
                case 4: result.add(new News("Head 4", "byline", "lead", "body", null));
                        break;
                case 5: result.add(new News("Head 5", "byline", "lead", "body", null));
                        break;
                case 6: result.add(new News("Head 6", "byline", "lead", "body", null));
                        break;
                case 7: result.add(new News("Head 7", "byline", "lead", "body", null));
                        break;
                case 8: result.add(new News("Head 8", "byline", "lead", "body", null));
                        break;
                case 9: result.add(new News("Head 9", "byline", "lead", "body", null));
                        break;
            }
        }
        return result;
    }
}
